import React from "react";
import GalleryPlugin from "react-grid-gallery";
import { altGallery } from "../common/imageAlt";
import { getUrl } from "../common/Image";

function Gallery({ galleries, id }) {
  const images = galleries.flatMap(item=>item.gallery).map((item, index) => {
    let thumbnailWidth = 320;
    let thumbnailHeight = 270;
    if(index % 3 == 0){
        thumbnailWidth =320;
        thumbnailHeight = 150; 
    }
    if(index % 2 == 0){
        thumbnailWidth =320;
        thumbnailHeight = 222; 
    }
    return({
    src: getUrl(item),
    thumbnail: getUrl(item),
    caption: altGallery,
    thumbnailWidth,
    thumbnailHeight,
  })});
  return (
    <div id={id} className="gallery">
      <div className="container">
        <h1 className="title">GALERIA</h1>
        <GalleryPlugin images={images}  margin={5}/>
      </div>
    </div>
  );
}

export default Gallery;

import React from 'react'
import Image,{getBackgroundImageUrl} from '../common/Image';
import { altLogo } from '../common/imageAlt';


function FirstPage({others,id}) {
    const {
        first_section_title,
        first_section_subtitle,
        logo,
        image_first
    } = others;

    return (
        <div 
        id={id}
        className="firstPage parallax"
        style={getBackgroundImageUrl(image_first)}
        >
            <div className="container">
                <div className="firstPage__tel"><span>tel.</span><a href="tel:+48 713333222"> 713-333-222 </a></div>
                <Image item={logo} alt={altLogo} className="firstPage__logo"  />
                <div className="firstPage__text">
                    <h1>{first_section_title}</h1>
                    <h1>{first_section_subtitle}</h1>
                </div>
            </div>
        </div>
    )
}

export default FirstPage

import React from "react";
import getFirstWord from "../common/getFirstWord";
import Image from "../common/Image";

function AboutMe({ others,id }) {
  const { about_me_title, about_me_description, image_aboutMe } = others;
  const { first, secend } = getFirstWord(about_me_title);
  return (
    <div id={id} className="aboutMe">
      <div className="aboutMe__content">
        <div className="container">
          <Image item={image_aboutMe} />
          <div>
            <h1>
              {first} <span style={{ fontWeight: "100" }}>{secend}</span>
            </h1>
            <div
              className="aboutMe__content--text"
              dangerouslySetInnerHTML={{ __html: about_me_description }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutMe;

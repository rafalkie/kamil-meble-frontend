import React, { useEffect, useState, useRef } from "react";
// import { Link } from "react-router";

function CheckedShort(props) {
  const { item, onOpenModal, index } = props;

  const [height, setHeight] = useState(0);

  const ref = useRef(index);

  useEffect(() => {
    setHeight(ref.current.clientHeight);
  });
  return (
    <>
      <div className="references__text">
        <div className="references__text__html">
          <div
            ref={ref}
            dangerouslySetInnerHTML={{ __html: item.description }}
          />
        </div>
        <div className="references__text__box"></div>
        {/* {height > 140 && (
          <Link
            className="references__text__read"
            onClick={() => onOpenModal(item._id)}
          >
            czytaj więcej...
          </Link>
        )} */}
      </div>
    </>
  );
}

export default CheckedShort;

import React from "react";
import Gallery from "./gallery";

function References({ references,id }) {
  return (
    <div id={id} className="references">
      <div className="container">
        <h1 className="title">REFERENCJE</h1>
        <Gallery references={references} />
      </div>
    </div>
  );
}

export default References;

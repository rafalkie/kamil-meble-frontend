import React from "react";
import AliceCarousel from "react-alice-carousel";
import CheckedShort from "./checkedShort";
import Modal from "react-responsive-modal";
import { url } from "../common/Image";
import { appUrlImg } from "../../panel/src/components/helper";

const options = { 
  mouseTrackingEnabled:true,
  dotsDisabled:true,
  buttonsDisabled:true,
  autoPlay:true,
  autoPlayInterval:3000,
  responsive:{
  0: { items: 1 },
  850: { items: 2 },
  1024: { items: 3 }
}};

function Gallery({references}) {
  const handleOnDragStart = (e) => e.preventDefault();
  return(
    <AliceCarousel  {...options}>
      {getItemGallery(references)}
     </AliceCarousel>
  )
  
}

export default Gallery;


const getItemGallery = (references) =>{

    return references.map((item, index) => (
      <div key={index} className="references__gallery">
        <CheckedShort
          onOpenModal={item => this.onOpenModal(item)}
          item={item}
          index={index}
        />
        <div className="references__gallery__author">
          {item.image !== undefined ? (
            <img
              src={`${url}/uploads/${item.image.folder}/${item.image.name}`}
              alt=""
              width="250px"
            />
          ) : (
            <img src={`/avatar.png`} alt="" width="250px" />
          )}
          <div>
            <p className="references__gallery__author--name">{item.name}</p>
          </div>
        </div>
      </div>
    ));
  
}
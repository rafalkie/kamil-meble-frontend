import React from "react";

export const url = process.env.API_URL;
export const appUrl = process.env.APP_URL;


export const getBackgroundImageUrl = (item) =>{
  return {backgroundImage:`url(${url}/uploads/${item && item.folder}/${item && item.name})`};
}
export const getUrl = (item) =>{
  return `${url}/uploads/${item && item.folder}/${item && item.name}`;
}

const Image = props => {
  const { item, alt, width = "", height = "", onLoad,className } = props;
  return (
    <div className={className}>
      {onLoad !== undefined ? (
        <img
          onLoad={onLoad}
          onError={onLoad}
          width={width}
          height={height}
          src={`${url}/uploads/${item && item.folder}/${item && item.name}`}
          alt={alt}
        />
      ) : (
        <img
          width={width}
          height={height}
          src={`${url}/uploads/${item && item.folder}/${item && item.name}`}
          alt={alt}
        />
      )}
    </div>
  );
};
export default Image;

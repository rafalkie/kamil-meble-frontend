import React, { useState } from "react";
import AnchorLink from "react-anchor-link-smooth-scroll";

function ScrollBar() {
  const [hover, setHover] = useState({
    active: false,
    index: 100
  });

  const tab = [
    "Strona główna",
    "Kim jesteśmy ?",
    "Oferta",
    "Referencje",
    "Galeria",
    "Kontakt"
  ];
  const toggleHover = id => {
    const copyHover = hover.active;
    setHover({
      active: !copyHover,
      index: !copyHover ? id : 100
    });
  };
  return (
    <div className="scrollBar">
      {tab.map((item, id) => (
        <div key={id} className="scrollBar__box">
          {hover.index === id ? (
            <p className="scrollBar__text">{item}</p>
          ) : null}
          <AnchorLink href={`#section-${id}`}>
            <div
              onMouseEnter={() => toggleHover(id)}
              onMouseLeave={() => toggleHover(id)}
              className="scrollBar__item"
            ></div>
          </AnchorLink>
        </div>
      ))}
    </div>
  );
}

export default ScrollBar;

const getFirstWord = sentence => {
    if (!sentence) return false;

    const splitSentence = sentence.split(" ");
    const first = splitSentence[0];
    const secend = sentence.slice(first.length);
    return { first, secend };
  };

  export default getFirstWord;
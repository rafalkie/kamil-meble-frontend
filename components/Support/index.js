import React from "react";
import {getBackgroundImageUrl} from "../common/Image";

function Support({ support,others,id }) {
  const {image_second} = others;
  return (
    <div 
    id={id}
    className="support parallax"
    style={getBackgroundImageUrl(image_second)}
    >
      <div className="container">
        <h1 className="title">OFERTA</h1>
        <div className="support__container">
          {support.map((item,index) => {
            const { title, description } = item;
            return (
              <div key={index} className="support__container--item">
                <h2>{title}</h2>
              { description &&  <p>{description}</p>}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default Support;

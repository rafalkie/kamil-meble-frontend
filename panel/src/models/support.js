import React, { useState } from "react";
import {
  List,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  DeleteButton,
  TextInput,
  FormDataConsumer
} from "react-admin";
import ModalFile from "../components/ModalFile";
import { Fragment } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import { imgUrl, cardStyle, exist } from "../components/helper";
import PhotoMax from "../components/layout/PhotoMax";
import CustomToolbar from "./../components/layout/CustomToolbar";
import PostActions from "../components/layout/PostAction";
import ListDropDown from "../components/dragAndDrop/ListDropdown";
import { useForm } from "react-final-form";

const SupportGrid = ({ ids, data, basePath }) => (
  <div style={{ margin: "1em" }}>
    {ids.map(id => (
      <Card key={id} style={cardStyle}>
        <CardHeader title={<TextField record={data[id]} source="title" />} />
        <CardContent className="card-content">
          {exist(data[id].image) && (
            <img
              src={`${imgUrl}${data[id].image.folder}/${data[id].image.name}`}
            />
          )}
        </CardContent>
        <CardActions
          style={{ display: "flex", justifyContent: "space-around" }}
        >
          <DeleteButton
            basePath={basePath}
            record={data[id]}
            resource="support"
          />
          <EditButton
            resource="support"
            basePath={basePath}
            record={data[id]}
          />
        </CardActions>
      </Card>
    ))}
  </div>
);

export function SupportList(props) {
  const [visible, setVisible] = useState(true);
  return (
    <>
      {visible ? (
        <div>
          <List
            {...props}
            sort={{ field: "position", order: "ASC" }}
            title="W czym pomagam"
            actions={
              <PostActions
                visible={visible}
                order={() => setVisible(!visible)}
                {...props}
              />
            }
          >
            <SupportGrid />
          </List>
        </div>
      ) : (
        <List
          {...props}
          sort={{ field: "position", order: "ASC" }}
          title="W czym pomagam"
          actions={
            <PostActions
              visible={visible}
              order={() => setVisible(!visible)}
              {...props}
            />
          }
        >
          <ListDropDown name={"title"} setVisible={() => setVisible(true)} />
        </List>
      )}
    </>
  );
}

const Origin = ({ formData, ...rest }) => {
  const form = useForm();
  const { task } = rest;
  return (
    <Fragment>
      <TextInput fullWidth source="title" label="Tytuł" />
      <TextInput fullWidth multiline source="description" label="Opis" />
      <ModalFile
        changePhoto={value => form.change("image_id", value)}
        {...rest}
        file={{
          title: "Zdjęcie",
          name: "image",
          type: "radio",
          task
        }}
      />
      <PhotoMax height="150px" width="220px" />
    </Fragment>
  );
};
export function SupportEdit(props) {
  return (
    <Edit title={<SupportTitle />} {...props}>
      <SimpleForm toolbar={<CustomToolbar />}>
        <FormDataConsumer>
          {formDataProps => (
            <Origin {...formDataProps} {...props} task="edit" />
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Edit>
  );
}

const SupportTitle = ({ record }) => {
  return <span>Edycja w czym pomagam {record ? `"${record.title}"` : ""}</span>;
};

function SupportCreate(props) {
  return (
    <Create title="Dodaj w czym pomagam" {...props}>
      <SimpleForm redirect="/support">
        <FormDataConsumer>
          {formDataProps => (
            <Origin {...formDataProps} {...props} task="create" />
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Create>
  );
}

export default SupportCreate;

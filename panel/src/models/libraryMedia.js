import React from 'react';
import File from '../plugin/File/component/file';
import UploadFile from '../plugin/File/component/uploadFile';
import Alert from '../components/alert';

const LibraryMedia = (props) => (
  <div className="layout">
      <>
        <UploadFile {...props} />
        <File visible={true} {...props} type="hidden" />
        <Alert />
      </>
  </div>
)

export default LibraryMedia;
// in src/Menu.js
import React, { useState } from "react";
import { connect, useSelector } from "react-redux";
import { MenuItemLink, getResources, usePermissions } from "react-admin";
import { withRouter } from "react-router-dom";
import OtherIcon from "@material-ui/icons/Bookmark";
import CommentIcon from "@material-ui/icons/Comment";
import FileCopy from "@material-ui/icons/FileCopy";
import GalleryIcon from "@material-ui/icons/PhotoLibrary";
import Accessibility from "@material-ui/icons/Accessibility";
import Pages from "@material-ui/icons/Pages";
import Forum from "@material-ui/icons/Forum";


const Menu = ({ dense, onMenuClick, logout }) => {
  const [state, setState] = useState({
    menuCatalog: false,
    menuSales: false,
    menuCustomers: false
  });
  const open = useSelector((state: AppState) => state.admin.ui.sidebarOpen);
  useSelector((state: AppState) => state.theme); // force rerender on theme change

  const handleToggle = (menu: MenuName) => {
    setState(state => ({ ...state, [menu]: !state[menu] }));
  };
  const { permissions } = usePermissions();
  return (
    <div>
      <MenuItemLink
        to="/support"
        primaryText="Realizacje"
        leftIcon={<Accessibility />}
        dense={dense}
      />
      <MenuItemLink
        to="/galleries/"
        primaryText="Galeria"
        leftIcon={<GalleryIcon />}
        dense={dense}
      />
      <MenuItemLink
        to="/references"
        primaryText="Referencje"
        leftIcon={<CommentIcon />}
        dense={dense}
      />
      <MenuItemLink
        to="/others/5e2df513740e572c48b52685"
        primaryText="Pozostałe"
        leftIcon={<OtherIcon />}
        dense={dense}
      />

      <MenuItemLink
        to="/files/"
        primaryText="Pliki"
        leftIcon={<FileCopy />}
        dense={dense}
      />
      {permissions === "admin" && (
        <MenuItemLink to="/users/" primaryText="Użytkownicy" dense={dense} />
      )}
    </div>
  );
};

const mapStateToProps = state => ({
  resources: getResources(state)
});

export default withRouter(connect(mapStateToProps)(Menu));

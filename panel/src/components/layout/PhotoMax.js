import React from 'react'

function PhotoMax(props) {
  const {width,height} = props;
  return (
    <div style={{padding:"15px 0 0 0"}}>
      Rozmiar zdjęć : (szer. {width}) , (wys. {height})
    </div>
  )
}

export default PhotoMax

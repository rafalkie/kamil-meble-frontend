import React, { Component } from 'react';
import { UserMenu, MenuItemLink } from 'react-admin';
import SettingsIcon from '@material-ui/icons/Settings';

class MyUserMenu extends Component {
    render() {
        const { crudGetOne, profile, ...props } = this.props;
        return (
            <UserMenu  {...props}>
                <MenuItemLink
                    to="/settings"
                    primaryText="Ustawienia"
                    leftIcon={<SettingsIcon />}
                />
            </UserMenu>
        );
    }
}


export default MyUserMenu;
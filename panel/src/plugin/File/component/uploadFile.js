import React, { useState, useEffect } from "react";
import Modal from "react-responsive-modal";
import actions from "./../action";
import { connect } from "react-redux";
import Folder from "@material-ui/icons/Folder";
import FolderOpen from "@material-ui/icons/FolderOpen";
import FolderCreate from "@material-ui/icons/CreateNewFolder";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import {
	editFolderService,
	newFolderService,
	deleteFolderService
} from "./../../../service/folders";
import { uploadFileService } from "./../../../service/files";

function UploadFile(props) {
	const { name, changePhoto, getSave } = props;
	const { typeInput, tabSelect, open, save } = props.list;

	const [state, setState] = useState({
		openNewFolder: false,
		changeNewFolder: false,
		nameNewFolder: "",
		oldNameFolder: ""
	});

	const openModalNewFolder = () => {
		setState({ ...state, openNewFolder: true });
	};
	const closeModalNewFolder = () => {
		setState({ ...state, openNewFolder: false });
	};
	const openModalChangeFolderName = (e, folder) => {
		setState({ changeNewFolder: true, oldNameFolder: folder });
	};
	const closeModalChangeFolderName = () => {
		setState({ ...state, changeNewFolder: false });
	};

	const changeNameFolder = (e, folder) => {
		setState({ ...state, nameNewFolder: e.target.value });
		folder && setState({ ...state, nameNewFolder: folder });
	};
	const newFolder = async () => {
		const { getAlert } = props;
		const { nameNewFolder } = state;
		await newFolderService(nameNewFolder)
			.then(() => {
				getAlert(true, "Folder dodany", true);
			})
			.catch((error) => {
				getAlert(true, "Folder isnieje juz o takiej nazwie", false);
			});
		props.getFolders();
		closeModalNewFolder();
	};

	const uploadFile = async (event) => {
		const { changeModal, getAlert, getFile } = props;
		const { itemsCountPerPage, activePage, activeFolder, data } = props.list;
		event.preventDefault();
		let date = new FormData(event.target);
		changeModal(false);

		await uploadFileService(date)
			.then(() => {
				getAlert(true, "Zdjęcie dodane", true);
			})
			.catch((error) => {
				getAlert(true, `Zdjęcie nie dodane -${error.response.data.message} `, false);
			});
		getFile(itemsCountPerPage, activePage, activeFolder);
	};

	const deleteFolder = async (folder) => {
		const { getAlert } = props;
		const { folders } = props.list;
		if (folders.length > 1) {
			await deleteFolderService(folder)
				.then(() => {
					getAlert(true, "Usunięto", true);
					props.clearTabSelected();
				})
				.catch((error) => {
					getAlert(true, `Nie usunięto -${error.response.data.message} `, false);
				});
			props.getFolders();
		} else {
			getAlert(
				true,
				"Nie możesz usunąć foldera , ponieważ przynajmniej jeden powinien istnieć.",
				false
			);
		}
	};
	const editFolder = async (item) => {
		const { getAlert } = props;
		const { oldNameFolder, nameNewFolder } = state;
		await editFolderService(oldNameFolder, nameNewFolder)
			.then(() => {
				getAlert(true, "Nazwa zmieniona", true);
			})
			.catch(() => {
				getAlert(true, "Nazwa nie zmieniona", false);
			});
		closeModalChangeFolderName();
		props.getFolders();
	};

	useEffect(() => {
		const { itemsCountPerPage, activePage, activeFolder } = props.list;
		props.getFile(itemsCountPerPage, activePage, activeFolder);
	}, [props.list.activeFolder]);

	useEffect(() => {
		props.getFolders();
	}, []);

	const { changeModal, changeActiveFolder, changePage, basePath } = props;
	let { folders, activeFolder } = props.list;
	const { openNewFolder, nameNewFolder, changeNewFolder } = state;

	return (
		<>
			<div className="folders">
				<button type="button" className="folder" onClick={openModalNewFolder}>
					<FolderCreate /> Nowy Folder
				</button>
				{folders.map((folder, index) => (
					<div key={index}>
						<ContextMenuTrigger id={folder}>
							<button
								type="button"
								id={index}
								className="folder"
								onClick={() => {
									changeActiveFolder(folder);
									changePage(1);
								}}
							>
								{activeFolder === folder ? <FolderOpen /> : <Folder />} {folder}
							</button>
						</ContextMenuTrigger>
						<ContextMenu id={folder}>
							<MenuItem data={{ foo: "bar" }} onClick={(e) => openModalChangeFolderName(e, folder)}>
								Zmień nazwę
							</MenuItem>
							<MenuItem data={{ foo: "bar" }} onClick={() => deleteFolder(folder)}>
								Usuń folder
							</MenuItem>
						</ContextMenu>
					</div>
				))}
			</div>
			<Modal open={openNewFolder} onClose={closeModalNewFolder} center>
				<div className="folder__modal">
					<label>Nazwa folderu:</label>{" "}
					<input type="text" value={nameNewFolder} onChange={changeNameFolder} />
					<button className="button" onClick={newFolder}>
						Dodaj
					</button>
				</div>
			</Modal>
			<Modal open={changeNewFolder} onClose={closeModalChangeFolderName} center>
				<div className="folder__modal">
					<label>Nazwa folderu:</label>{" "}
					<input type="text" value={nameNewFolder} onChange={changeNameFolder} />
					<button className="button" onClick={editFolder}>
						Zmień nazwę
					</button>
				</div>
			</Modal>
			<Modal open={open} onClose={() => changeModal(false)} center>
				<form className="files__form" onSubmit={(event) => uploadFile(event)}>
					<input type="hidden" name="folder" value={activeFolder} />
					<input type="file" name="files" multiple={true} />
					<input type="submit" className="button" value="Dodaj" />
				</form>
			</Modal>
		</>
	);
}
const mapStateToProps = (state) => ({
	list: state.files
});
const mapDispatchToProps = (dispatch) => {
	return {
		changePage: (activePage) => dispatch(actions.changePage(activePage)),
		changeActiveFolder: (folders) => dispatch(actions.changeActiveFolder(folders)),
		getFolders: () => dispatch(actions.getFolders()),
		getFile: (itemsCountPerPage, activePage, activeFolder) =>
			dispatch(actions.getFile(itemsCountPerPage, activePage, activeFolder)),
		changeModal: (status) => dispatch(actions.changeModal(status)),
		getAlert: (display, message, status) => dispatch(actions.getAlert(display, message, status)),
		clearTabSelected: () => dispatch(actions.clearTabSelected())
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(UploadFile);

import React, { useEffect, useState } from "react";
import Pagination from "react-js-pagination";
import DeleteIcon from "@material-ui/icons/Delete";
import AddAPhoto from "@material-ui/icons/AddAPhoto";
import actions from "./../action";
import { connect } from "react-redux";
import { delFileService } from "./../../../service/files";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";

function File(props) {
	const {
		changeType,
		type,
		clearTabSelected,
		getFile,
		changeModal,
		changePage,
		addSelectItem,
		name,
		changeCountPerPage
	} = props;
	const {
		activePage,
		itemsCountPerPage,
		activeFolder,
		tabSelect,
		data,
		totalItemsCount,
		typeInput
	} = props.list;
	const [tabSelectId, setTabSelectId] = useState({});

	useEffect(() => {
		changeType(type);
		if (type === "radio") {
			setTabSelectId({});
		} else {
			setTabSelectId({ [name]: [] });
		}
	}, []);

	useEffect(() => {
		getFile(itemsCountPerPage, activePage, activeFolder);
	}, [props.list.activePage, itemsCountPerPage]);

	useEffect(() => {
		if (tabSelect[name] !== undefined) {
			setTabSelectId(
				tabSelect[name].length > 0
					? { [name]: tabSelect[name].map((elem) => elem._id) }
					: { [name]: [] }
			);
		}
	}, [props.list.tabSelect]);

	useEffect(() => {
		changeType(props.type);
	}, [props.list.typeInput]);
	const delFile = async (id) => {
		const { itemsCountPerPage, activePage, activeFolder } = props.list;
		const { getAlert, getFile } = props;

		await delFileService(id)
			.then(() => {
				Object.entries(tabSelect).map((item) =>
					item[1]._id === id ? addSelectItem(undefined, undefined, undefined, {}, item[0]) : null
				);
				getAlert(true, "Zdjęcie usunięte", true);
			})
			.catch(() => {
				getAlert(true, "Błąd podczas usuwania", false);
			});
		getFile(itemsCountPerPage, activePage, activeFolder);
	};
	return (
		<>
			<div className="managerFile">
				<div className="files">
					<button type="button" className="add" onClick={() => changeModal(true)}>
						<AddAPhoto /> <p>Dodaj Zdjęcie</p>
					</button>
					{data.map((item, index) => (
						<label
							key={item._id + Math.floor(Math.random() * (100 + index)) + 12}
							className="files__item"
							htmlFor={item._id}
						>
							<button type="button" name="delete" className="del" onClick={() => delFile(item._id)}>
								<DeleteIcon />
							</button>
							{typeInput !== "hidden" ? (
								type === "radio" ? (
									<input
										checked={tabSelect[name] && item._id === tabSelect[name]._id}
										type={typeInput}
										name="foto"
										id={item._id}
										onChange={(e) => addSelectItem(e, item, typeInput, tabSelect, name)}
									/>
								) : type === "checkbox" ? (
									<input
										checked={tabSelectId[name] && tabSelectId[name].includes(item._id)}
										type={typeInput}
										name="foto"
										id={item._id}
										onChange={(e) => addSelectItem(e, item, typeInput, tabSelect, name)}
									/>
								) : null
							) : null}
							<p className="files__item--name">{item.originalName}</p>
							<div className="card-content">
								<img
									src={`${process.env.REACT_APP_API}/uploads/${item.folder}/${item.name}`}
									alt=""
								/>
							</div>
						</label>
					))}
				</div>
			</div>
			<Pagination
				activePage={activePage}
				itemsCountPerPage={itemsCountPerPage}
				totalItemsCount={totalItemsCount}
				onChange={changePage}
			/>
			<div className="files__perPage">
				<InputLabel>Zdjęć na strone: </InputLabel>

				<FormControl>
					<Select value={itemsCountPerPage} onChange={(e) => changeCountPerPage(e.target.value)}>
						<MenuItem value={5}>5</MenuItem>
						<MenuItem value={10}>10</MenuItem>
						<MenuItem value={20}>25</MenuItem>
						<MenuItem value={30}>50</MenuItem>
					</Select>
				</FormControl>
			</div>
		</>
	);
}

const mapStateToProps = (state) => ({
	list: state.files
});
const mapDispatchToProps = (dispatch) => {
	return {
		changeCountPerPage: (count) => dispatch(actions.changeCountPerPage(count)),
		changeType: (typeInput) => dispatch(actions.changeType(typeInput)),
		changePage: (activePage) => dispatch(actions.changePage(activePage)),
		getFile: (itemsCountPerPage, activePage, activeFolder) =>
			dispatch(actions.getFile(itemsCountPerPage, activePage, activeFolder)),
		clearTabSelected: () => dispatch(actions.clearTabSelected()),
		delFile: (id) => dispatch(actions.delFile(id)),
		addSelectItem: (e, item, howMuch, tabSelect, symbol) =>
			dispatch(actions.addSelectItem(e, item, howMuch, tabSelect, symbol)),
		changeModal: (status) => dispatch(actions.changeModal(status)),
		getAlert: (display, message, status) => dispatch(actions.getAlert(display, message, status))
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(File);

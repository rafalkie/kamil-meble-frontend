import React from "react";
import { Admin, Resource } from "react-admin";
import { Route } from "react-router-dom";
import feathersClient from "./feathersClient";
import { restClient, authClient } from "ra-data-feathers";
import { ReferenceList, ReferenceCreate, ReferenceEdit } from "./models/references";
import { UsersList, UsersEdit, UsersCreate } from "./models/users";
import { OtherEdit } from "./models/others";
import myTheme from "./components/thema";
import { i18nProvider, MyLoginPage } from "./components/client";
import Usersicon from "@material-ui/icons/Person";
import { GalleryList, GalleryEdit, GalleryCreate } from "./models/galleries";
import LibraryMedia from "./models/libraryMedia";
import { createBrowserHistory as createHistory } from 'history';
import Settings from "./models/settings";
import MyAppBar from "./components/MyAppBar";
import { Layout } from "react-admin";
import Menu from "./components/layout/Menu";
import "./style/index.scss";
import SupportCreate, { SupportList, SupportEdit } from "./models/support";
import { Provider } from "react-redux";
import createAdminStore from "./components/createAdminStore";


export const token = localStorage.getItem("feathers-jwt");

const restClientOptions = {
	id: "_id", // In this example, the database uses '_id' rather than 'id'
	usePatch: false // Use PATCH instead of PUT for updates
};
const authClientOptions = {
	storageKey: "feathers-jwt",
	authenticate: { strategy: "local" },
	permissionsField: "userroles",
	logoutOnForbidden: true,
	redirectTo: "/login"
};
const MyLayout = (props) => <Layout {...props} appBar={MyAppBar} />;
const authProvider = authClient(feathersClient, authClientOptions);
const history = createHistory();
export const dataProvider = restClient(feathersClient, restClientOptions);

const App = () => (
	<Provider
		store={createAdminStore({
			authProvider,
			dataProvider,
			history
		})}
	>
		<Admin
			appLayout={MyLayout}
			menu={Menu}
			theme={myTheme}
			locale="pl"
			loginPage={MyLoginPage}
			title="Name App"
			dataProvider={dataProvider}
			authProvider={authProvider}
			i18nProvider={i18nProvider}
			history={history}
			customRoutes={[<Route key="settings" path="/settings" component={Settings} />]}
		>
			{(permissions) => [


			
				<Resource
					name="references"
					options={{ label: "Referencje" }}
					list={ReferenceList}
					edit={ReferenceEdit}
					create={ReferenceCreate}
				/>,
				<Resource
					name="support"
					options={{ label: "W czym pomagam" }}
					list={SupportList}
					edit={SupportEdit}
					create={SupportCreate}
				/>,
			
				<Resource
					name="galleries"
					options={{ label: "Galeria" }}
					list={GalleryList}
					edit={GalleryEdit}
					create={GalleryCreate}
				/>,
				<Resource name="others" options={{ label: "Pozostałe" }} edit={OtherEdit} />,
				<Resource name="files" list={LibraryMedia} options={{ label: "Pliki" }} />,

				permissions === "admin" ? (
					<Resource
						name="users"
						list={UsersList}
						edit={UsersEdit}
						create={UsersCreate}
						options={{ label: "Użytkownicy" }}
						icon={Usersicon}
					/>
				) : null
			]}
		</Admin>
	</Provider>
);

export default App;

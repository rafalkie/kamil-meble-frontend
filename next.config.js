require('dotenv').config()
const withSass = require('@zeit/next-sass')

module.exports =  withSass({
    env:{
        "APP_URL": process.env.APP_URL,
        "API_URL": process.env.API_URL,
    },
})
import React from "react";
import axios from "axios";
import References from "../components/References";
import AboutMe from "../components/AboutMe";
import Support from "../components/Support";
import Gallery from "../components/Gallery";
import Contact from "../components/Contact";
import FirstPage from "../components/FirstPage";
import "./../assets/styles.scss";
import ScrollBar from "../components/common/scrollBar/scrollBar";
import Messenger from "../components/common/Messenger";

function App({ data: { others, support, references, galleries } }) {
  return (
    <>
      <FirstPage others={others} id="section-0" />
      <AboutMe others={others} id="section-1" />
      <Support support={support} others={others} id="section-2" />
      <References references={references} id="section-3" />
      <Gallery galleries={galleries} id="section-4" />
      <Contact others={others}  id="section-5"/>
      <ScrollBar />
      <Messenger pageId={others.messenger_id} version="5.0" />
    </>
  );
}
App.getInitialProps = async function() {
  const result = await axios
    .all([
      getServiceAxios("others"),
      getServiceAxios("support"),
      getServiceAxios("references"),
      getServiceAxios("galleries")
    ])
    .then(
      axios.spread((others, support, references, galleries) => {
        return {
          others: others.data[0],
          support: support.data.data,
          references: references.data.data,
          galleries: galleries.data.data
        };
      })
    );
  return {
    data: result
  };
};

export default App;

function getServiceAxios(name) {
  return axios.get(`${process.env.API_URL}/${name}`);
}
